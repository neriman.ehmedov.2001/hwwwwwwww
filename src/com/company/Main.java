package com.company;

public class Main {
    public static void main(String[] args) {
        Human mother = new Human("Rahima", "Ahmadov", 2000);
        Human father = new Human("Neriman", "Ahmadov", 2001);

        Family familyAhamdov = new Family(mother, father);
        mother.setFamily(familyAhamdov);
        father.setFamily(familyAhamdov);

        Human esma = new Human("Esma", "Ahmadov", 2020, 160);

        esma.setSchedule("Monday", "go to courses; watch a film");
        esma.setSchedule("Sunday", "do home work");
        esma.setSchedule("Wednesday", "do workout");
        esma.setSchedule("Friday", "read e-mails");
        esma.setSchedule("Saturday", "do shopping");
        esma.setSchedule("Thursday", "visit grandparents");
        esma.setSchedule("Tuesday", "do household");

        familyAhamdov.addChild(esma);
        esma.setFamily(familyAhamdov);

        Pet esmasPet = new Pet("dog", "Rex", 5, 49);
        esmasPet.setHabits("eat");
        esmasPet.setHabits("drink");
        esmasPet.setHabits("sleep");
        familyAhamdov.setPet(esmasPet);
        esma.greetPet();
        esma.describePet();
        esmasPet.respond();
        esmasPet.eat();
        esmasPet.foul();

        System.out.println(esmasPet.toString());

        System.out.println(esma.toString());

        Human child1 = new Human();
        Human child2 = new Human();
        Human child3 = new Human();
        familyAhamdov.addChild(child1);
        familyAhamdov.addChild(child2);
        familyAhamdov.addChild(child3);
        familyAhamdov.countFamily();

        System.out.println();

        familyAhamdov.deleteChild(2);
        familyAhamdov.countFamily();

        System.out.println();

        familyAhamdov.deleteChild(child3);
        familyAhamdov.countFamily();

        System.out.println();
        System.out.println();
        System.out.println(familyAhamdov.getChildren());

    }
}