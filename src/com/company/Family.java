package com.company;

import java.util.*;

enum Species {
    Dog,
    DomesticCat,
    RoboCat,
    Fish,
    UNKNOWN;
}

class Family {
    // field
    private Human mother, father;
    private ArrayList<Human> children = new ArrayList<Human>();
    private HashSet<Pet> pet = new HashSet<Pet>();

    // constructors
    public Family() {}
    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    // methods
    public int countFamily() {
        int members = 2 + children.size();
        System.out.println("Family members are: " + members);
        return members;
    }
    public void addChild(Human child) {
        this.children.add(child);
    }
    public boolean deleteChild(int index) {
        if (index > children.size()) {
            System.out.println("You write wrong index");
            return false;
        }

        children.remove(index);

        System.out.println("The child has been deleted.");
        return true;
    }
    public boolean deleteChild(Human child) {
        children.remove(child);

        System.out.println("The child has been deleted.");
        return true;
    }

    // override methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return Objects.equals(getMother(), family.getMother()) &&
                Objects.equals(getFather(), family.getFather()) &&
                Objects.equals(getPet(), family.getPet()) &&
                Objects.equals(children, family.children);
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(getMother(), getFather(), getPet());
        result = 31 * result + Objects.hashCode(children);
        return result;
    }
    @Override
    public String toString() {
        return "Family{" +
                "mother = " + mother +
                ", father = " + father +
                ", pet = " + pet +
                ", children = " + children + '}';
    }

    // setters
    public void setMother(Human mother) {
        this.mother = mother;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public void setPet(Pet pet) {
        this.pet.add(pet);
    }

    // getters
    public Human getMother() {
        return mother;
    }
    public Human getFather() {
        return father;
    }

    public Pet getPet() {
        return null;
    }

    public ArrayList<Human> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public void setPet(HashSet<Pet> pet) {
        this.pet = pet;
    }
}
